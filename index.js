// BASE SETUP
// ==============================================

var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

app.use(methodOverride());

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(errorHandler);

var port = process.env.PORT || 8080;        // set our port

app.get('/',function(req,res){
  res.send("Hi there, this is a get response from Kai Huang");
});

app.post('/',function(req,res){
	if(req.body.payload){
		res.send(prepareResponse(req.body.payload));
	}else{
		res.status(400);
		res.send({ error: "Unexpected JSON structure." });
	}
  
});

function errorHandler(err, req, res, next) {
  res.status(400);
  res.send({ error: "Could not decode request: JSON parsing failed" });
}

function prepareResponse(payload){
	var response = [];
	for(var index=0; index<payload.length;index++){
			if(payload[index].drm && payload[index].episodeCount >0){
				var element = {};
				element.image=payload[index].image != null ? payload[index].image.showImage:"";
				element.slug=payload[index].slug;
				element.title=payload[index].title;
				response.push(element);
			} 
	}
	return {"response":response};
}


// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Server started on port ' + port);